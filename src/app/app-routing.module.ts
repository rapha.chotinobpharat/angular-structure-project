import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenGuard } from '@core/guards/authen-guard.guard';

const routes: Routes = [
  {
    path: 'home',
    canActivate: [AuthenGuard],
    loadChildren: () => import(`./modules/root/home/home.module`).then(m => m.HomeModule),

  },
  {
    path: 'customer',
    canActivate: [AuthenGuard],
    loadChildren: () => import(`./modules/root/customer/customer.module`).then(m => m.CustomerModule),

  },
  {
    path: 'login',
    loadChildren: () => import(`./core/authentication/login/login.module`).then(m => m.LoginModule),

  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
