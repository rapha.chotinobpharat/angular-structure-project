import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-sum',
  templateUrl: './sum.component.html',
  styleUrls: ['./sum.component.scss']
})
export class SumComponent {

  disabledBtn = false;

  sumValue(num1: number, num2: number) {
    return num1 + num2;
  }

}
