import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SumComponent } from './sum.component';

describe('SumComponent', () => {
  let component: SumComponent;
  let fixture: ComponentFixture<SumComponent>;
  let btnSubmit;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SumComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    btnSubmit = fixture.debugElement.nativeElement.querySelector('#submit_btn');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be 3', () => {
    expect(component.sumValue(1, 2)).toEqual(3);
  });

  it('Should be create button', () => {
    expect(btnSubmit).toBeTruthy();
  });

  it('Should be disabled when form not valid', () => {
    component.disabledBtn = true;
    fixture.detectChanges();
    expect(btnSubmit.disabled).toBeTruthy();
  });

  it('Should be enabled when form not valid', () => {
    component.disabledBtn = false;
    fixture.detectChanges();
    expect(btnSubmit.disabled).toBeFalsy();
  });
});
