import { Component } from '@angular/core';
import { CustomerService } from '@core/services/customer.service';
import { ICustomerList } from '@interface/customer';
import { take } from 'rxjs';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers: [CustomerService]
})
export class CustomerComponent {

  constructor(private readonly customerService: CustomerService) {

  }

  getList() {
    this.customerService.getCustomerList()
    .pipe(take(1))
    .subscribe((response: ICustomerList[]) => {
      // do something
    });
  }

}
