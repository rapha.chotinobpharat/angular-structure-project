export interface ICustomerList {
  name: string;
  lastName: string;
  isActive: boolean;
}
