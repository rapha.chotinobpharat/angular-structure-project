import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';

const components = [LoginComponent];

@NgModule({
  declarations: components,
  exports: components,
  imports: [
    CommonModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
