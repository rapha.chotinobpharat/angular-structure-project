import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../http/api.service';
import { Injectable } from '@angular/core';
import { ICustomerList } from '@interface/customer';

@Injectable()
export class CustomerService extends ApiService {

  constructor(http: HttpClient) {
    super(http);
  }

  getCustomerList(): Observable<ICustomerList[]> {
    return this.get('path');
  }
}
