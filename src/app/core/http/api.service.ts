import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { localStorageKey } from '@shared/localStorageKey';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiService {

  private url = environment.domain;

  constructor(private readonly http: HttpClient) {
  }

  get(path: string, userToken = true): Observable<any> {
    const headers = this.getHeader(userToken);
    return this.http.get(`${this.url}${path}`, { headers });
  }

  post(path: string, payload: any, userToken = true): Observable<any> {
    const headers = this.getHeader(userToken);
    return this.http.post(`${this.url}${path}`, payload, { headers });
  }

  put(path: string, payload: any, userToken = true): Observable<any> {
    const headers = this.getHeader(userToken);
    return this.http.put(`${this.url}${path}`, payload, { headers });
  }

  delete(path: string, userToken = true): Observable<any> {
    const headers = this.getHeader(userToken);
    return this.http.delete(`${this.url}${path}`, { headers });
  }

  getAccessToken(): string {
    return localStorage.getItem(localStorageKey.token) || '';
  }

  private getHeader(userToken = true) {
    let headers = new HttpHeaders();
    if (userToken && this.getAccessToken()) {
      headers.append('Authorization', `Bearer ${this.getAccessToken()}`);
    }
    return headers;
  }
}
